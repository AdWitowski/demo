package exercise_stack;

public interface Stack<T> {
	
	public T pop();
	public T peek();
	public void push(T element);
	public boolean isEmpty();
}
