package exercise_stack;
import exercise_doubly_linked_list.DoublyLinkedList;

public class StackImpl<T> implements Stack<T> {
	
	private DoublyLinkedList<T> doublyList;
	
	public StackImpl() {
		doublyList = new DoublyLinkedList<>();
	}

	@Override
	public T pop() {
		return doublyList.deleteLast();
		
	}

	@Override
	public T peek() {
		
		return doublyList.get(doublyList.length() - 1);
	}

	@Override
	public void push(T element) {
		doublyList.append(element);
		
	}

	@Override
	public boolean isEmpty() {
		
		return doublyList.length() == 0;
	}

}
