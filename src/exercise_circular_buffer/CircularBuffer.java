package exercise_circular_buffer;

public class CircularBuffer<T> {
	private T[] elements;
	private int nextRead;
	private int nextWrite;
	private int numberOfElements;

	@SuppressWarnings("unchecked")
	public CircularBuffer(int initialSize) {
		elements = (T[]) new Object[initialSize];
		nextRead = 0;
		nextWrite = 0;
		numberOfElements = 0;
	}

	public T dequeue() {
		if (isEmpty()) {
			throw new IllegalStateException();
		}
		T element = elements[nextRead];
		elements[nextRead] = null;
		nextRead++;
		numberOfElements--;
		return element;
	}

	public void enqueue(T element) {
		if (isFull()) {
			throw new IllegalStateException();
		} else if (nextWrite == elements.length) {
			nextWrite = 0;
		}
		elements[nextWrite] = element;
		nextWrite++;
		numberOfElements++;
	}

	public boolean isFull() {
		return elements.length == numberOfElements;
	}

	public boolean isEmpty() {
		if (numberOfElements == 0) {
			return true;
		}
		return false;
	}
}
