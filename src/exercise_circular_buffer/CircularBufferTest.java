package exercise_circular_buffer;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CircularBufferTest {
	
	private CircularBuffer<Integer> circularBuffer;

	@Before
	public void setUp(){
		circularBuffer = new CircularBuffer<>(5);
	}
	
	@Test
	public void dequeue_works_correctly(){
		circularBuffer.enqueue(5);
		circularBuffer.enqueue(2);
		circularBuffer.dequeue();
		
		assertListsEqual(circularBuffer, 2);
	}
	
	@Test
	public void enqueue_works_correctly(){
		circularBuffer.enqueue(5);
		circularBuffer.enqueue(3);
		
		assertListsEqual(circularBuffer, 5,3);
	}
	
	
	@Test(expected = IllegalStateException.class)
	public void should_throw_exception_when_enqueue_and_queue_is_full(){
		CircularBuffer<Integer> buffer = new CircularBuffer<>(3);
		buffer.enqueue(1);
		buffer.enqueue(2);
		buffer.enqueue(3);
		buffer.enqueue(4);
	}
	
	@Test(expected = IllegalStateException.class)
	public void should_throw_exception_when_dequeue_and_queue_is_empty(){
		CircularBuffer<Integer> buffer = new CircularBuffer<>(3);
		buffer.dequeue();
	}
	
	@Test
	public void queue_works_correctly(){
		CircularBuffer<Integer> buffer = new CircularBuffer<>(3);
		assertTrue(buffer.isEmpty());
		assertFalse(buffer.isFull());
		buffer.enqueue(1);
		assertFalse(buffer.isEmpty());
		assertFalse(buffer.isFull());
		buffer.enqueue(2);
		buffer.enqueue(3);
		assertFalse(buffer.isEmpty());
		assertTrue(buffer.isFull());
		
		assertEquals((Integer) 1, buffer.dequeue());
		assertEquals((Integer) 2, buffer.dequeue());
	}
	
	
	
	private void assertListsEqual(CircularBuffer<Integer> array, int... elements){
		for(int i=0;i<elements.length;i++){
			assertEquals(elements[i], (int) array.dequeue());
		}
	}
	
	
}

