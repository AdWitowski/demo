package exercise_singly_linked_list;
public class SinglyLinkedList<T> {
	private int length;
	private Node first;
	
	public void append(T element){
		insert(length, element);
	}
	
	public void prepend(T element){
		insert(0, element);
	}
	
	public void insert(int index, T element){
		if(index > length || index < 0){
			throw new IndexOutOfBoundsException(String.valueOf(index));
		}
		Node newNode = new Node(element, null);
		if(index == 0){
			Node newSecond = first;
			first = newNode;
			first.next = newSecond;
		} else{
			Node previous = first;
			Node current = first.next;
			for(int i=1;i<index;i++){
				previous = current;
				current = current.next;
			}
			previous.next = newNode;
			newNode.next = current;
		}
		length++;
	}
	
	public T delete(int index){
		checkRange(index);
		T value = null;
		if(index == 0){
			value = first.next.value;
			first = first.next;
		} else{
			Node current = first.next;
			Node previous = first;
			for(int i=2;i<=index;i++){
				Node newCurrent = current.next;
				previous = current;
				current = newCurrent;
			}
			value = current.value;
			previous.next = current.next;
		}
		length--;
		
		return value;
	}

	private void checkRange(int index) {
		if(index > length || index < 0){
			throw new IndexOutOfBoundsException();
		}
	}
	
	public T get(int index){
		checkRange(index);
		return getNode(index).value;
	}
	
	private Node getNode(int index){
		Node current = first;
		for(int i=0;i<index;i++){
			current = current.next;
		}
		return current;
	}
	
	public T deleteFirst(){
		return delete(0);
	}
	
	public T deleteLast(){
		return delete(length - 1);
	}
	
	public int length(){
		return length;
	}
	
	private class Node{
		T value;
		Node next;
		
		public Node(T value, Node next){
			this.value = value;
			this.next = next;
		}
	}
	
}
