package exercise_doubly_linked_list;

public class DoublyLinkedList<T> {
	private int length;
	private Node first = null;
	private Node last = null;

	private class Node {
		T value;
		Node previous;
		Node next;

		public Node(T value, Node next, Node previous) {
			this.value = value;
			this.next = next;
			this.previous = previous;
		}

	}

	public void append(T element) {

		Node tmpNode = new Node(element, null, last);
		if (last != null) {
			last.next = tmpNode;
			last = last.next;
		}

		if (first == null) {
			first = tmpNode;
			last = first;

		}
		length++;

	}

	public int length() {
		return length;
	}

	public T get(int index) {
		if (index < 0 || index > length) {
			throw new IndexOutOfBoundsException();
		}
		Node current = first;
		for (int i = 0; i < index; i++) {
			current = current.next;
		}
		return current.value;
	}

	public T deleteFirst() {
		T value = first.value;
		first = first.next;
		first.previous = null;
		length--;
		return value;
	}

	public T deleteLast() {
		T value = last.value;
		last = last.previous;
		last.next = null;
		length--;
		return value;
	}

	public T delete(int index) {
		if (index < 0 || index > length) {
			throw new IndexOutOfBoundsException();
		} else if (index == 0) {
			deleteFirst();
		} else if (index == length) {
			deleteLast();
		}
		T value = get(index);
		Node current = first;
		for (int i = 0; i < index; i++) {
			current = current.next;
		}
		current.previous.next = current.next;
		current.previous = null;
		current.next = null;

		length--;
		return value;

	}

	public void insert(int index, T element) {
		if (index == 0) {
			prepend(element);
		}
		else if (index < 0 || index > length) {
			throw new IndexOutOfBoundsException();
		} else {
			Node current = first;
			for (int i = 0; i < index; i++) {
				current = current.next;
			}
			Node newNode = new Node(element, current, current.previous);

			current.previous.next = newNode;
			current.previous = newNode;
			length++;
		}

	}

	public void prepend(T element) {
		Node tmpNode = new Node(element, first, null);
		if (first != null) {
			first.previous = tmpNode;
		}
		first = tmpNode;
		if (last == null) {
			last = tmpNode;
		}
		length++;

	}
}
