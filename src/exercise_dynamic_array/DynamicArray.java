package exercise_dynamic_array;

//� void append(T element) // dodaj na koniec
//� void prepend(T element) // dodaj na poczatku
//� void insert(int index, T element) // wstaw w okreslonym miejscu
//� T delete(int index)
//� T get(int index)
//� int length()

public class DynamicArray<T> {

	private T[] elements;
	private int firstEmpty;

	@SuppressWarnings("unchecked")
	public DynamicArray(int initialSize) {
		elements = (T[]) new Object[initialSize];
		firstEmpty = 0;

	}

	public void extendArraySize() {

		// elements = Arrays.copyOf(elements, elements.length * 2);

		@SuppressWarnings("unchecked")
		T[] newElements = (T[]) new Object[elements.length * 2];
		for (int i = 0; i < elements.length; i++) {
			newElements[i] = elements[i];
		}
		elements = newElements;

	}

	public void append(T element) {
		if (elements.length == firstEmpty) {
			System.out.println("Tablica jest za ma�a. Nast�puje jej zwi�kszenie");
			extendArraySize();
		}
		elements[firstEmpty] = element;
		firstEmpty++;
		for (T el : elements) {
			System.out.println(el);

		}
		System.out.println("----------------------");

	}

	public void prepend(T element) {
		if (elements.length == firstEmpty) {
			System.out.println("Tablica jest za ma�a. Nast�puje jej zwi�kszenie");
			extendArraySize();
		}
		System.arraycopy(elements, 0, elements, 1, firstEmpty);
		elements[0] = element;
		firstEmpty++;

		// for (int i = firstEmpty; i > 0; i--) {
		// elements[i] = elements[i - 1];
		// }
		// elements[0] = element;
		// firstEmpty++;

		for (T el : elements) {
			System.out.println(el);

		}
		System.out.println("----------------------");

	}

	public void delete(int index) {
		System.arraycopy(elements, index + 1, elements, index, firstEmpty - index);
		firstEmpty--;
		for (T el : elements) {
			System.out.println(el);

		}
		System.out.println("----------------------");

	}

	// wstaw w okreslonym miejscu
	public void insert(int index, T element) {
		if (elements.length == firstEmpty) {
			System.out.println("Tablica jest za ma�a. Nast�puje jej zwi�kszenie");
			extendArraySize();
		} else if (index < 0 || index > firstEmpty) {
			throw new ArrayIndexOutOfBoundsException();
		}
		System.arraycopy(elements, index, elements, index + 1, firstEmpty - index);
		elements[index] = element;
		firstEmpty++;
		for (T el : elements) {
			System.out.println(el);

		}
		System.out.println("----------------------");

	}

	public int length() {
		return firstEmpty;

	}

	public T get(int index) {
		return elements[index];
	}

}